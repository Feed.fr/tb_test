#include <iostream>
#include <string>
#include <set>

void ReverseString(char* s)
{
    if (!s)
    {
        return;
    }
    char* end = s;
    while (*end)
    {
        ++end;
    }
    --end; // remove the terminator

    for (; s < end; ++s, --end)
    {
        char b = *s;
        char e = *end;
        *s = e;
        *end = b;
    }
}


int binarySearch(int* sortedArray, int low, int high, int findValue)
{
    int mid = (low + high) / 2;
    if (sortedArray[mid] == findValue)
    {
        return mid;
    }
    if (sortedArray[mid] > findValue)
    {
        return binarySearch(sortedArray, low, mid - 1, findValue);
    }
    return binarySearch(sortedArray, mid + 1, high, findValue);
}


int FindInteger(int* sortedArray, int arrayLength, int findValue)
{
    int low = 0;
    int high = arrayLength;

    return binarySearch(sortedArray, low, high, findValue);
}


std::set<std::set<int>> getNumbersList(const char* number)
{
    std::set<std::set<int>> numbersList;
    std::set<int> allValues;
    std::string numberString = number;
    int maxSize = numberString.length();

    for (int currentSize = 1; currentSize <= maxSize; ++currentSize)
    {
        for (int currentPos = 0; currentPos < maxSize; ++currentPos)
        {
            std::string substring = numberString.substr(currentPos, currentSize);
            int i = atoi(substring.c_str());
            allValues.insert(i);
        }
    }
 
    for (auto i : allValues)
    {
        std::cout << i << std::endl;
        // got all unique values, now build the sets
        for (int charIndex = 0; charIndex < maxSize; ++charIndex)
        {
            std::string stringValue = std::to_string(i);
            if (stringValue[0] == number[charIndex])
            {
                //
            }
        }

    }
 
    return numbersList;
}

void solveEquation(const char* numbers, int answer)
{
    auto numbersList = getNumbersList(numbers);

    std::set<int> toto = { 12, 3, 45 };
    numbersList.insert(toto);

    for (auto l : numbersList)
    {
        int sum = 0;
        for (auto n : l)
        {
            sum += n;
        }
        if (sum == answer)
        {
            std::string output = "";
            for (auto m : l)
            {
                output += std::to_string(m);
                output += "+";
            }
            output.pop_back();
            std::cout << output << std::endl;
            return;
        }
    }
    std::cout << "UNSOLVABLE" << std::endl;
}

int main(int argc, char** argv)
{
    //std::cout << "toto" << std::endl;
    //std::string toto = "super!";
    //char* test = &toto[0];

    //std::cout << test << std::endl;
    //ReverseString(test);
    //std::cout << test << std::endl;

    //int array[10] = { 0, 7 ,18 ,23 ,52 ,73 ,128, 232, 758, 962 };
    //int (*sArray)[10] = &array;
    //int* sortedArray = sArray[0];

    //int value = 73;
    //int pos = FindInteger(sortedArray, 10, value);
    //std::cout << "position of " << value << " is " << pos << std::endl;

    std::string numbers = "12345";
    //getNumbersList(numbers.c_str());
    solveEquation("12345", 60);
}